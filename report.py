from datetime import timedelta, date
import json
import requests


timesheet = {}
timesheet["begin"]=""
timesheet["end"]=""
timesheet["project"] = 1
timesheet["activity"] = 1 
timesheet["description"] = "automated report"
timesheet["fixedRate"] = 0 
timesheet["hourlyRate"] = 0  

# Set in headers usename and kimai token
headers = { 
    'X-AUTH-USER': 'narenas' , 
    'X-AUTH-TOKEN': 'n1c0p4ul1' ,  
    'accept': 'application/json' , 
    'Content-Type': 'application/json'}


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)



start_date = date(2021, 11, 30)
end_date = date(2021, 12, 21)
start_hour = "08:30:00"
end_hour = "20:00:00"

holidays = [
    date(2021,12, 6) , 
    date(2021,12, 7) , 
    date(2021,12, 8)
] 
for single_date in daterange(start_date, end_date):

    if (single_date.weekday() == 5) or (single_date.weekday() == 6): 
        continue 

    if single_date in holidays: 
        continue 
    timesheet["begin"] = single_date.strftime("%Y-%m-%dT" + start_hour)
    timesheet["end"] = single_date.strftime("%Y-%m-%dT" + end_hour)
    timesheet_json = json.dumps(timesheet)
    r = requests.post("https://fichaje.qindel.com/api/timesheets"  , headers=headers,  data = timesheet_json) 
    print (r.json())



