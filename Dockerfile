FROM python:slim 

ADD report.py /python/report.py
ADD requirements.txt /python/requirements.txt
RUN pip install -r /python/requirements.txt

